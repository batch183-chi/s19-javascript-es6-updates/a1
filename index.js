// console.log("Hello World");

let getCube = 2;

let result = 2 ** 3;

console.log(`The cube of ${getCube} is ${result}` );

let address = [258, "Washington Ave", "NW", "California", 90011];

let [streetNumber, streetName, city, state, zipCode] = address;

console.log(`I live at ${streetNumber} ${streetName} ${city} ${state} ${zipCode}`);

let animal = {
	animalName: "Lolong",
	animalType: "saltwater crocodile",
	animalWeight: "1075 kgs",
	animalLength: "20 ft 3 in"
}

let {animalName, animalType, animalWeight, animalLength} = animal;

console.log(`${animalName} was a ${animalType}. He weighed at ${animalWeight} with a measurement of ${animalLength}.`)

let numbers = [1, 2, 3, 4, 5];

numbers.forEach((number) => {
	console.log(`${number}`);
} )

let reduceNumber = numbers.reduce((acc, current) => acc+current, 0);

console.log(reduceNumber);

class Dog{
	constructor(name, age, breed){
		this.name = name;
		this.age = age;
		this.breed = breed;
	}
}

let dogA = new Dog("Frankie", 5, "Miniature Dachshund");

console.log(dogA);



